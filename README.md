# SecureFlag Open Platform

The SecureFlag Platform is a training platform created for developers to learn and practice modern secure coding techniques through hands-on exercises. The platform helps develop secure coding skills through real-world challenges to ensure knowledge acquired during the course can be confidently and continuously applied in the real world.

<br/>

<div align="center">
The SecureFlag Open Platform is an <a target="_blank" href="https://owasp.org/www-project-secureflag-open-platform">OWASP Project and includes an SDK and developer tools to create Labs for the SecureFlag platform.</a>


<p align="center"><img width="200px" src="img/owasp.png"/></p>
</div>

<p align="center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/24KrcgjsBaw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</p>
<br/>

Developers manually find, exploit and remediate the code of vulnerable applications running in development environments accessed via a web browser. The platform offers 100% hands-on training, with no multiple-choice questions involved, and uses an engine able to live-test code changes to measure efficacy, instantly displaying whether the code has been fixed and awarding points upon exercise completion. 

SecureFlag provides a gamified experience where users can assemble into teams, track their progress through a leaderboard and unlock special challenges. Exercises are grouped into sequences of logically-linked units called Learning Paths that enable participants to attain expert, usable knowledge in relatively small steps. When candidates complete a Learning Path, they receive a SecureFlag certification; certifications have an expiration date and can be renewed by taking refresher exercises throughout the year. 

Armed with their newly embedded capacities, developers can take part in time-boxed tournaments and engage a whole community of participants from within the same organisation, all competing to remediate security issues.
 

## Contacts

[openplatform@secureflag.com](mailto:openplatform@secureflag.com)