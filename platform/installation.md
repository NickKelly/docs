

# SecureFlag Open Platform Installation



1. **Create an AWS Account**<br>
   Reference the [tutorial](https://aws.amazon.com/premiumsupport/knowledge-center/create-and-activate-aws-account/) to create a new AWS account. If you already have an AWS account, skip this step. 



2. **Choose in which AWS Region to deploy**<br>
   Choose an AWS Region for your main deployment. All AWS Services required by SecureFlag are available in all AWS Regions. Ideally you should choose the AWS region that is closest to your users.

   

3. **Register a New Domain** <br>
   To deploy an instance of SecureFlag Open Platform, you will need to have a domain. AWS Route 53 allows one to register domain names starting from 8 USD; please reference the [tutorial](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-register.html).  Skip this step if you already have a domain name and the ability to create subdomains and manage DNS records.

   

4. **Get SSL/TLS Certificate for Domain**<br>
   - You can request a publicly trusted TLS certificate issued by AWS Certificate Manager or import an existing certificate. On the AWS Console, browse to AWS Certificate Manager.
   -  Click on `Request a Certificate`; in the next screen, select `Request a Public Certificate`, then add the domain names. We recommend creating two entries; one for the base FQDN (e.g. `mysecureflag.com`) another for all its subdomains (`*.mysecureflag.com`) as shown below:
      ![Add Domain Names](../img/sf-domain-names.png)
   -  Click on `Next` then select `DNS Validation` and click on `Next`. In the next screen (Tags), click on `Review`, then `Confirm and Request`

   -  You must complete the validation steps before AWS ACM can issue the certificate.

   	![DNS Validation](../img/sf-acm-validation.png)

   -  In Route 53, add a CNAME field to your DNS configuration for each distinct entry above: 

     ![Route53 DNS Validation](../img/route53-validation.png)

   -  Once completed, <u>save the ARN</u> for the generated/imported certificate. Note: you need to request the ACM certificate in the same AWS region where you will be deploying the Platform. For step-by-step instructions, please reference the official AWS [tutorial](https://docs.aws.amazon.com/acm/latest/userguide/gs-acm-request-public.html) on how to request a new certificate.

     ![](../img/sf-domain-arn.png)

5. **Create Key Pair** <br>
    To allow for SSH access to the EC2 instances created as part of the SecureFlag deployment, you need to generate a Key Pair. Reference the [tutorial](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-key-pairs.html#having-ec2-create-your-key-pair) and <u>save the KeyPair's name.</u>
    **Note**: you need to create a KeyPair in the same AWS region where you will be deploying the Platform.
    
    By default, SSH access to the EC2 instances is disabled. To enable it, in the EC2 Security Groups whitelist access port TCP/22 from your IP address for the EC2 instances that you require SSH access to.

   

6. **Import and tweak AWS CloudFormation template**<br>
   On the AWS console, in your chosen AWS Region for deployment, browse to CloudFormation. You will need to import, tweak and run the [SecureFlag Platform Template](https://secureflag-ce.s3.eu-west-2.amazonaws.com/sf-template-master.yaml). This will automatically deploy SecureFlag's infrastructure and launch all services.

   - Click on `Create New Stack`

   - Ensure `Template is Ready` and `Specify an Amazon S3 Template Url` are selected, then enter SecureFlag Open Platform's template:

     ```
     https://secureflag-ce.s3.eu-west-2.amazonaws.com/sf-template-master.yaml
     ```

     ![select_template](../img/sf-choose-template.png)

   - Click on `Next`

   - Tweak the template filling in the items in red to match your configuration using the Parameters Configuration Table below:

     ![Fill In Cloudformation Parameters](../img/sf-fill-parameters.png)

   

   ​	Parameters Configuration Table
   
   | Parameter               | Description                                                  |
   | ----------------------- | ------------------------------------------------------------ |
   | Stack name              | Name of the deployed stack (e.g. `SF-Open`)                  |
   | GatewayHostname         | FQDN for the Exercise Gateway (e.g. `gateway.mysecureflag.com`) |
   | KeyPair                 | Name of the `KeyPair` created at step 5                      |
| PortalAdminUserPassword | Initial password for the `admin` account. Note: password change is prompted on first authentication. |
   | PortalHostname          | FQDN for the Portal (e.g. `www.mysecureflag.com`)            |




7. **Run AWS CloudFormation template**<br>

   - Click on `Next`

   - Scroll down and click on `Next`

   - Scroll down and check both `I acknowledge that AWS CloudFormation might create IAM resources with custom names` and `I acknowledge that AWS CloudFormation might require the following capability: CAPABILITY_AUTO_EXPAND`

     ![](../img/sf-capabilities.png)

   - Click on `Create Stack`

   - Wait ~ 14 minutes

   - When the stack is created, click on the root stack name (e.g. SF-Open) then click on "Outputs" and save the value for `LoadBalanceUrl` which is the URL for the Application Load Balancer.

     ![ALB Output](../img/sf-alb-output.png)
     
     

8. **Modify DNS Records**<br>

   - Modify the DNS records for your domain, adding a `CNAME` for:<br> SecureFlag's Portal (e.g. `www.mysecureflag.com`)
   -  SecureFlag's Gateway (e.g. `gateway.mysecureflag.com`) 

   Both DNS records need to point to the Application Load Balancer's URL retrieved at the previous step.

   If your domain name is managed by AWS, you can use Route 53 to manage its DNS records as shown below: 	

   - In the AWS Console, browse to Route 53. Click on `DNS Management` in the Hosted Zone and select the zone corresponding to the domain you specified at step 6.

   - Add a CNAME record for SecureFlag's Portal with `name` matching the FQDN chosen for the Parameter `PortalHostname` at step 6 and `value` set to the FQDN for the Load Balancer (ALB) retrieved at step 7.

   - Add a CNAME record for SecureFlag's Gateway with `name` matching the FQDN chosen for the Parameter `GatewayHostname` at step 6 and `value` set to the FQDN for the Load Balancer (ALB) retrieved at step  7.
     ![Setup DNS](../img/sf-setup-dns.png)

9. **Visit the Portal**<br>
   Open one of the supported browsers (Chrome, Firefox, Safari, Microsoft Edge, Internet Explorer 11) and visit the address chosen for `PortalHostname`. Login with the user `admin` and the password chosen for `PortalAdminUserPassword`. You will be prompted to change the credentials at the first authentication.

