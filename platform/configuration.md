

# First-time Configuration



1. **Create Organisations**

   The Platform comes with the sample `Org` organisation configured.

   Organisations model a company or a division within a company. Users can be part of one organisation; management users can manage multiple organisations. 

   ![Add Organization](../img/sf-add-organization.png)
   
   Through the management interface, admins can generate codes to invite users to join the Platform.
   
      ![oranization_details](../img/sf-organization-details.png)



2. **Add Users**

   The Platform comes with the `admin` user as the only registered user.

   Add Users to a specific Organisation specifying user's role and other details (keep Concurrent Exercises Limit set to 1 for the current release).

   *Available Roles:*

   - SF Admin: <br>
   Install/Update/Delete Exercises & Learning Paths
   - Organisation Admin:<br>
   Create/Update/Remove Organizations, Teams, Users, Tournaments
   - Team Manager:<br>
   Review Exercises for managed teams, setup Tournaments for managed teams, access data for users in managed teams
   - Stats Monitor:<br>
   Access Metrics, access User, Teams, Tournaments data
   - User:<br>
   Run exercises, join Tournaments, view completed exercises, personal/team stats and achievements.
   
   ![Add User](../img/sf-add-user.png)

   



​      

2. **Create Teams**

   The Platform comes with a sample `Team A` with the `admin` user as the only member.

   Through the management interface, you can create Teams and add Users to them. Teams, like Users, belong to one Organisation. A User can be added to only one Team. 
   Team Managers can review exercises completed by members of their teams. Team Managers can also add/remove users to/from their Teams.

   ![Add Team](../img/sf-add-team.png)   

   
