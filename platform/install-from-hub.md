

# Install Exercises From Exercise Hub

The SecureFlag Platform allows you to install new Exercises and Learning Paths from the Exercise Hub. 

The following steps assume you have already [deployed](installation.md) and [configured](configuration.md) your SecureFlag instance. 

If you're looking to create new SecureFlag Exercises, follow the steps detailed in the [SDK](../sdk/setup-sfsdk) section.



1. **Login to your SecureFlag Open Platform instance with an SFAdmin user.**

   Authenticate to the Portal with your `SFAdmin` user; this is the `admin` user that is created with the installation. It is also possible to add additional `SFAdmin` users.

2. **Browse to the Exercise Hub tab.** 

   Click on `Hub` in the management interface of your SecureFlag instance.  

   ![Exercise Hub](../img/sf-exercise-hub.png)



3. **Browse by Technologies and select an Exercise.** 

   Browse available Exercises and select one that is marked as `Not Installed`; click on `More Details`.
   
   ![Select Exercise](../img/sf-select-exercise.png)



4. **Install the Exercise**

   Select all Gateways and all the Organisations that you wish to make this exercise available to (if you are not sure, it is safe to enable them all). Then click on `Install`.![Install Exercise](../img/sf-install-hub-exercise.png)

   

5. **Wait for Installation**
   Wait for the Exercise to download and install. This process occurs in the background and can take up to 10 minutes. You can keep track of current downloads by checking the `Download Queue` in the `Hub` tab. If the `Download Queue` indicator is not visible, there aren't exercises in queue.
   
   ![Wait for Installation](../img/sf-download-queue.png)



6. **Confirm Exercise is correctly installed**

   Browse to "Available Exercise", search for the title of the installed exercise and ensure the status is `Available`.

   ![Wait for Installation](../img/sf-post-install-1.png)

   Click on `Details` and  ensure the exercise has at least one Gateway configured and that it is enabled for all the required Organisations. An Exercise can be played by Users in an Organisation if the Exercise is enabled for that Organisation.

   ![Ensure Gateways and Organization Are Setup](../img/sf-post-install-2.png)



### Troubleshooting:



**Exercise Stuck in Queue**

If you have had an Exercise pending in the Queue for some time, click on the `Restart` button to restart the installation progress. If this does not resolve, click on the `Remove` button in the Queue then browse to `Available Exercises`, delete the exercise and install it again from the `Hub` tab. 



**Queue is Empty but Exercise's status Listed as 'Download Queue'**

Browse to `Available Exercises`, delete the exercise and install it again from the `Hub` tab. 

