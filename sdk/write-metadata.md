# Add Exercise Metadata

The Exercise Metadata includes description, flags, points, duration, hints, solution and other information necessary to run an Exercise on the SecureFlag platform. 

Exercise Metadata and Image are bundled together in the distribution phase, when the exercise is either published to the Exercise Hub or deployed to a SecureFlag Platform instance.  

A SecureFlag Exercise Image can be used to run multiple exercises creating different Exercise Metadata. Create a vulnerable application affected by many security issues and create an Exercise Metadata for each vulnerability.

<p align="center"><img src="../img/sf-multi-exercise-image.png"/></p>

The exercise's metadata can be edited either via the management interface of SecureFlag Platform if you're a `sfadmin` user or it can be edited using the `sfsdk exercise-edit` command.

### First Login

At this point, you'll need a valid SecureFlag account. Developers can [signup](mailto:openplatform@secureflag.com) on the SecureFlag Exercise Hub.

After you obtain a valid account, run `sfsdk config` to setup the credentials.

```console
$ sfsdk config
[!] Warning: this saves your credentials in /Users/emilio/sf/login.yml in an insecure way. Skip this (press Ctrl-C) to be prompted when authenticating.

[*] Please type the username for your Hub account
Username: dev@my.org
[*] Please type emilio@secureflag.com's password
Password:
```

> WARNING: `sfsdk config` stores the clear-text credentials under `~/sf/login.yml` . This file can be read by anyone in the system running with your privilege level or above. Do not run `sfsdk config` if you do not wish to save your credentials on your local machine.

### Run the exercise editor

The SDK embeds a local web editor to write and manage exercises' metadata.

```shell
 $ sfsdk exercise-edit
 
[+] Logged in the SecureFlag Hub as user developer@myorg.com

 * Serving Flask app "sfsdk.web" (lazy loading)
 * Environment: production
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Open your browser to [http://localhost:5000](http://localhost:5000).

### Add a new exercise

Before writing the metadata for the new exercise, go to `Exercise > KBs` to browse the public Knowledge Base (KB) that the SDK downloads from the source you're logged in. The Knowledge Base contains: 

* **Vulnerabilities**: List of information regarding a type of vulnerability. They can be agnostic or technology-related. Agnostic and technology-specific KB are concatenated when used in an exercise.

* **Technology Stacks**: List of instructions on how to use a specific Technology Stack. They usually describe how to use the IDE and start the application  on a specific technology image.

* **Frameworks**: List of language frameworks.


To start creating the metadata for a new exercise, click on the navigation bar `Exercise > Add New exercise` . The `Add Exercise` dialogue will open to fill in the details of the new exercise.

![New exercise](../img/sf-new-exercise.png)

#### Description

The description tab contains the exercise title, topics, author, and exercise description. It also defines metadata such as max duration of the exercise, difficulty, technology, technology variant, and framework. The technology fields dropdown menus refer to the Technology Stacks available in the KB.

#### Info

The info tab displays the informations regarding the Technology Stack selected in the Description tab and allows to write optional additional information about the exercise.

#### Flags

The flags tab manages the list of flags for an exercise. The flags are the challenges that a player must solve to complete the exercise and score points and trophies.

Every flag has a title, vulnerability type, flag type, instructions, hint, max score, hint reduction, self-checks status, and message. The flag instructions and hint support Markdown text.

The vulnerability menu refers to the Vulnerabilities available in the KB.

The flags mapping translates the checks strings contained in `checks.yml` as described in the [Write Exercise Checks](/sdk/create-image#write-exercise-checks) section. It maps  `vulnerable`, `fixed`, etc. to human readable messages. 

![mapping](../img/sf-mapping.png)

#### Solution

Markdown text that contains the exercise solution.

#### Trophy

Name of the trophy that the player wins if the exercise is completed with score >= 100%.

#### Tags

List of optional tags.

