# Create Lab Image

A SecureFlag's Lab Image is a Docker image that contains an intentionally vulnerable application and all the development tools required by the trainee to run the virtualized Lab on the platform.

This guide provides step by step instructions on how to create a new SecureFlag image named `org-php-hackme` based on the SecureFlag's PHP base image `secureflag/sf-php`.

> You can find the resulting image of this training at the [org-php-hackme](https://gitlab.com/secureflag-community/org-php-hackme/) repository.

## Image Development

The image development lifecycle is the process of creating a new, custom SecureFlag image to host your vulnerable application and run SecureFlag exercises on. The picture below summarises the development process.

![Develoment Lifecycle](../img/sf-dev-lifecycle.png)

### Initialise the New Image

Initialise an empty structure for a new image named `org-php-hackme` based on the image `sf-php` .

```console
$ sfsdk img-add org-php-hackme
[+] New image org-php-hackme has been added at '~/sf/img/org-php-hackme'
```

The command above creates minimal SecureFlag images' settings under `~/sf/images.yml` and initialises the build folder and the `Dockerfile` for your new image under `~/sf/img/org-php-hackme/`. 

Use `sfsdk img-ls` to list the available images to see the image you just created.

```console
$ sfsdk img-ls
Name                      Running   Container name   RDP port   RDP creds                                                                                                    
--------------------------------------------------------------------------
org-php-hackme 				No
```

### Build the Image

In the previous initialisation, we created a minimal image to host your vulnerable application. The base structure of the build directory of the project is the following:

```shell
~/sf/img/org-php-hackme/
   ├── Dockerfile
   └── fs/
```

Try a first build. This command results in running `docker build` on the build folder.

```console
$ sfsdk img-build org-php-hackme

[*] Building exercise image org-php-hackme...
Step 1/3 : FROM secureflag/sf-community

Step 2/3 : COPY fs/ /

Step 3/3 : RUN chown -R sf:sf /home/sf

Successfully tagged org-php-hackme:latest
[+] Build complete
```

As per the `Dockerfile`, any file in the `fs/` folder will be applied over the image's filesystem at build time, allowing any kind of customization.

### Run the Container for Development

Try to run the empty image launching the last build with `sfsdk img-run`. This executes `docker run` using the images' settings stored in `~/sf/images.yml`.

```shell
$ sfsdk img-run org-php-hackme
[+] Container org-php-hackme-inst has been started

  Container name:     org-php-hackme-inst  
  RDP port:           127.0.0.1:3389       
  RDP credentials:    sf:password          
  Application port:   127.0.0.1:8050
```

Verify that the container is running with `sfsdk img-ls`.

```bash
$ sfsdk img-ls
Name                 Running Container name              RDP port   RDP creds                                                                      
--------------------------------------------------------------------------------
org-php-hackme    Yes     org-php-hackme-inst   3389       sf:password 
```

### Configure the Environment

You'll need to install and configure anything you need to reproduce a fully configured environment, such as an IDE, a software packages, desktop and IDE configurations etc. like in the following example.

#### RDP into the desktop

The RDP address and credentials are displayed when the container is started or running `sfsdk img-ls`. The default values are:

* RDP address: `127.0.0.1:3389`
* Username: `sf`
* Password: `password`

Use an RDP client of your choice (e.g. Remmina on Linux or Microsoft Remote Desktop on macOS) to log into the running container. You should see the SecureFlag Desdktop with the browser installed.

#### Customize the container

Install any missing software, take note of the changes you do in order to be reproducible by the next Docker build.

For the purpose of this tutorial, we'll setup a minimal installation with Visual Studio Code IDE and a PHP web server that starts at boot.

Start by running `sfsdk img-shell` to obtain a root shell on the running container.

```console
$ sfsdk img-shell org-php-hackme

root:/#
```

Run the following commands (valid at the time this document was written) will install VSCode and PHP on the container.

```shell
curl -L 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64' -o /tmp/visualstudio.deb && \
  dpkg -i /tmp/visualstudio.deb && \
  rm -f /tmp/visualstudio.deb && \
  apt-get -y install php-cli
```

You might also want to add a VSCode launcher on the desktop by linking its application `.desktop` file.

```shell
ln -s /usr/share/applications/code.desktop /home/sf/Desktop/code.desktop
```

VSCode launcher requires an additional argument to run in a Docker container. Use your favoure terminal editor and modify `/usr/share/applications/code.desktop` by changing the `Exec` parameter as below.

```ini
Exec=/usr/share/code/code --no-sandbox --unity-launch %F
```

Observe that now clicking the launcher on the Desktop spawn a new VSCode window.

Finally, create an init file to run a PHP web server at the start of the container. Create a file with the following content at `/SF/init/enabled/10-php-server`, make it executable with `chmod +x /SF/init/enabled/10-php-server`.

```bash
#!/bin/bash

php -t /home/sf/exercise/app -S 0.0.0.0:80 &
```

To try the web server without rebuilding it, create the web root with `mkdir -p /home/sf/exercise/app` and run it manually by executing `/SF/init/enabled/10-php-server` from the root shell.

#### Copy the customization to the build folder

Once you are satisfied with the setup in your container, you must reproduce the changes you did in the previous section into the Docker build context.

Replicate the command to install VSCode and PHP to `~/sf/img/org-php-hackme/Dockerfile` as a `RUN` command.

```docker
RUN curl -L 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64' -o /tmp/visualstudio.deb && \
  dpkg -i /tmp/visualstudio.deb && \
  rm -f /tmp/visualstudio.deb && \
  apt-get -y install php-cli
```

Then, use `docker cp` to copy `code.desktop` and `10-php-server` files out of the container into the `fs` build directory.

```shell
cd ~/sf/img/org-php-hackme/
mkdir -p fs/usr/share/applications/ fs/home/sf/Desktop/
docker cp org-php-hackme-inst:/usr/share/applications/code.desktop fs/usr/share/applications/code.desktop
docker cp org-php-hackme-inst:/home/sf/Desktop/code.desktop fs/home/sf/Desktop/code.desktop # Ignore the warning here
```

```shell
cd ~/sf/img/org-php-hackme/
mkdir -p fs/SF/init/enabled/
docker cp org-php-hackme-inst:/SF/init/enabled/10-php-server fs/SF/init/enabled/10-php-server
```

Apply this approach for any other customization you want to copy out to your build folder in order to be reproduced in the next build.

#### Install the application

After configuring the environment, let's add the vulnerable application.

For the purpose of this tutorial, we're going to write a simple single-file PHP application called `hackme.php` that is vulnerable to Cross-Site Scripting. The application will be located within **/home/sf/exercise/app** in order to be run bhy the PHP server.

To install the example HackMe application, follow the steps from the RDP connection:

1. Switch to VSCode.

2. Click "File" > "Open Folder" and use the dialogs to create and open a new directory `~/exercise/app` in the home folder.

2. Expand the `app` directory on the left-side menu and right click it to add a new file named `index.php`

3. Write or copy the sample vulnerable code below in the `index.php` PHP script. The code below is vulnerable to Reflected Cross-Site Scripting.

   ```php
   <html>
   
     <h1>
       Hello! The time is <?php echo date("h:i:sa"); ?> 
     </h1>
   
     Click <a href="<?php echo($_SERVER['PHP_SELF']); ?>">here</a> 
     to reload the page.
   
   </html>
   ```

4. Reload the browser to make sure the PHP web server you started before correctly run the PHP application.

   ![hackme](../img/sf-hackme.png)

5. Experiment with the vulnerable app to check everything works as expected.

6. Once you are satisfied with your changes, you can export the app in the build folder by using `docker cp`.

  ```shell
  cd ~/sf/img/org-php-hackme/
  mkdir -p fs/home/sf/exercise/
  docker cp org-php-hackme-inst:/home/sf/exercise/app fs/home/sf/exercise/app
  ```

### Export other changes

Saving changes of configuration files in the home directory might be tricky. To help with this, the SDK comes with `sfsdk img-watch` to keep track of what you change in the home folder during the customisation of the environment.

```shell
 $ sfsdk img-watch org-php-hackme

[+] Please wait, preparing sf-devops-inst to run img-watch..
[+] Started watching changes in /home/sf folder..
```

Keep this command running on a terminal while you configure e.g. the IDE or the browser. For the sake of this tutorial, open VSCode on the `index.php` folder and close it. You'll notice a change has been detected.

```
[+] Changed files and folders:

  /home/sf/.config/
```

Once you have completed the customization, you can exit the `sfsdk img-watch` command with Ctrl-C. 

Use this information with `docker cp` command to copy the changed files from the running container you're working on to the build folder, and then rebuild the image again.

```shell
docker cp org-php-hackme-inst:/home/sf/.config fs/home/sf/.config
```

Alternatively, you can use `sfsdk img-snapshot` to automatically create a new image as a snapshot. The new image is the copy of the current exercise build folder that includes the directories that were detected as having changed.

### Stop the Container

Stop the container, build and run the new image and check that it behaves as expected.

```bash
$ sfsdk img-stop org-php-hackme
```

### Next Steps

Repeat the steps until the Desktop environment, the IDE and the vulnerable application work as expected. During this process, you will be iterating through the following steps:

1. Build the image with `sfsdk img-build`.

2. Run the image with `sfsdk img-run`.

3. Install and setup the software in the container while updating the Dockerfile and `fs` directory accordingly with the changes you want to see in the next build.

5. Stop the old image using `sfsdk img-stop`.

6. Build and restart the new version of the image.

When you have reached a satisfactory integration, add the checks, write [exercise metadata](write-metadata.md) and then [publish](distribute-exercises.md) your SecureFlag exercise.

## Write Lab Checks

SecureFlag Platform provides the Automated Checker, an engine that tests, in an automated fashion, whether the user correctly remediated the vulnerability in the exercise.

The automated checks for vulnerabilities and other system metrics is described by the `/SF/conf/checks.yml` YAML file inside the container.

> To read about the Automated Checker files structure, browse to the [Lab Checks Reference](/sdk/checks-reference.md).

### Vulnerability checks

The checks are designed to perform *real* tests on the functionality, such as injecting a payload to check if the vulnerability is still present. The same engine is also used to retrieve other system metrics such as the application server status.

The `flags` field in the YAML file defines the automated tests to check the status of a vulnerability. The test of a single flag is defined by a list of steps. The steps are executed sequentially until one of them is evaluated as `true`. It returns a string like `vulnerable`, `fixed`, `broken-functionality`, `broken-webserver` that can be mapped in the exercise metadata to give a feedback to the user regarding the status of the exercise.

The checks for the sample Reflected XSS vulnerability in the HackMe application can be written as follows:

```yaml
flags:
  xss-index:
    - status: vulnerable
      url: http://localhost/index.php/<img src=x onerror=alert(1);>
      assert: |
        '<img src=x onerror=alert(1);>' in res.text
    - status: fixed
      url: http://localhost/index.php/<img src=x onerror=alert(1);>
      assert: |
        '&lt;img src=x onerror=alert(1);&gt;' in res.text
    - status: broken-functionality
      assert: True
```

When the SecureFlag Platform queries the Automated Checker to verify what the vulnerability status of the exercise is, all the checks will execute sequentially until one is verified. The check returns `vulnerable` if the payload is reflected without being sanitised, `fixed` if HTML encoding has been applied correctly, or `broken-functionality` for any other unexpected scenario.

These strings must be mapped with human-friendly error messages when writing the [exercise metadata](write-metadata.md).

### Check binary

To help you developing the exercise checks, obtain a root shell on the development container and check the status by using `/SF/bin/check` utility like in the following example.

```shell
$ sfsdk img-shell org-php-hackme
root@org-php-hackme $ /SF/bin/check xss-index
{
  "errors": [], 
  "results": {
    "xss-index": "fixed"
  }
}
root@org-php-hackme $ /SF/bin/check app-status
{
  "errors": [], 
  "results": {
    "app-status": "running"
  }
}
```

Adjust `/SF/conf/checks.yml` until the command returns the expected results depending on the vulnerability and application statuses.

Set the `DEBUG` environment variable to `1` to enable the debug prints.

Make sure to add the file `checks.yml` in the `sf/SF/conf/` directory of your image before it gets published.


