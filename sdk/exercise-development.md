# Intro to Labs Development

### Structure of a SecureFlag Lab

A SecureFlag Lab is made up of two components: the Metadata and the Image. 

The Metadata includes description, flags, points, duration, hints, solution and other information related to the platform. 

The Image is a custom Docker Image that extends a SecureFlag base image. The base image contains a minimal base line that can be extended to add an intentionally vulnerable application together with the automated checks to establish if a running Lab is fixed or still vulnerable.

Lab Metadata and Image are bundled together in the distribution phase, when the Lab is either published to the Hub.  

A single Image run multiple Labs, each Lab is one Metadata. In this way is possible to create a vulnerable application affected by many security issues and create an Lab Metadata for each vulnerability.

<p align="center"><img src="../img/sf-exercise-structure.png"/></p>

### Steps to Create a new SecureFlag Lab

The steps illustrated below show the flow to create a new SecureFlag Lab. Reference the rest of the SDK documentation to learn how to [create an Lab image](create-image.md) and add a vulnerable application with [checks](write-checks.md) to it. Then [write Lab Metadata](write-metadata.md) for each vulnerability present in the Vulnerable App in the Lab Image. When ready, [publish](distribute-Labs.md) the Lab to Lab Hub. 

<p align="center"><img src="../img/sf-exercise-creation-process.png"/></p>




