# Publish/Deploy Lab

Once you have developed a fully working SecureFlag Image and written the metadata of your exercise, you can publish the Lab to the SecureFlag Lab Hub. 

### Image and Lab Metadata Identifiers

To distribute an exercise, you'll need to identify the image's `name` and the `ID` of the Lab Metadata you want to distribute. 

List the local images running `sfsdk img-ls` and pick the target image name you're building e.g. `org-php-hackme` .

```console
$ sfsdk img-ls
  Name                   Running    Container name   RDP port   RDP creds  Application port  
------------------------------------------------------------------------------------------- 
  org-php-hackme         No              
  org-java-brokencrm     No                                                                         
  org-php-sampleapp      No
```

Use `sfsdk exercise-ls` to list the local exercise's metadata and <u>note the local ID</u>. 

```shell
$ sfsdk exercise-ls 
  php_default_reflected_cross_site_scripting-sfsdk-1262
```

The same exercise metadata `ID` can also be displayed by running `sfsdk exercise-edit` and pointing your browser to [http://localhost:5000](http://localhost:5000), then browsing to the `Labs` tab.

![image-20200415135623243](../img/sf-exercise-id.png)

### Publish to SecureFlag Lab Hub

In order to publish an exercise to the Hub, you need a Developer account on the SecureFlag Lab Hub. 

> Please note that your published images will be vetted. Read the [contributing guidelines](/contribute.md) before publishing.

Use `sfsdk publish` to bundle together the image  `org-php-hackme`  and the exercise metadata `php_default_reflected_cross_site_scripting-sfsdk-1262` .

```bash
$ sfsdk publish \
	--exercise-name php_default_reflected_cross_site_scripting-sfsdk-1262 \
	--image-name org-php-hackme

  Logged in the SecureFlag hub as user 'developer@myorg.com'

  Lab has been published as 997354328093-developer@myorg.com-1587488380.zip
```