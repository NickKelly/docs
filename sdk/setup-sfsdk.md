# SDK Installation

The command line tool `sfsdk` provides an SDK to develop new SecureFlag exercises. The tool allows the development and submission of new Labs for the SecureFlag platform.

### Prerequisites

The following prerequisites are required to develop Labs for the SecureFlag platform.

1. A computer running recent versions of Linux or MacOS.
2. [Docker](https://docs.docker.com/get-docker/).
3. [Python version >= 3.6](https://www.python.org/downloads/).
4. An RDP client of your choice.
5. A `developer` account on the [SecureFlag Exercise Hub](https://hub.secureflag.com).

### Install

Install the SDK using `pip3`.

```shell
$ pip3 install --user sfsdk
```

If the `sfsdk` command is not available in your shell, you'll need to add pip's `bin` path to your `$PATH` environment variable. 

**Linux**

```shell
$ echo 'PATH="$PATH:$HOME/.local/bin"' >> ~/.profile
```

**MacOS**

```shell
$ echo 'PATH="$PATH:$HOME/Library/Python/3.7/bin/"' >> ~/.profile 
```

In both cases, `~/.profile` must be reloaded to be used in the current shell.

```shell
$ source ~/.profile
```

This path might differ depending your Python configuration.

### Workspace

Developing new Labs with `sfsdk` will populate the `~/sf` folder using the structure shown below:

```shell
~/sf/
  login.yml               # YAML with credentials
  images.yml              # YAML with images info
  img/                    # Directory containing images
    org-java-vapp/				# Build directory
      Dockerfile          # Dockerfile
      fs/                 # Other files
  srcs/                   # Directory containing labs metadata
	  default/
	   exr/
      java_xss/
      python-sqli/
```

