# Exercise Checks Reference

The YAML file `/SF/conf/checks.yml`  defines the automated tests to check the status of a vulnerability and the state of the application.


### Vulnerability checks

The `flags` field in the YAML file defines the automated tests to check the status of a vulnerability.

It returns a string like `vulnerable`, `fixed`, `broken-functionality`, `broken-webserver`, that can be mapped in the exercise metadata to give feedback to the user regarding the status of the exercise.

#### Flags

The `flags` object may contain an arbitrary number of check objects; each check object uses a short name that suggests the name of the vulnerability and the affected component e.g. `xss-index` .

Every check object must contain a list of steps. The steps are executed sequentially until one of them is evaluated as `true`.

#### Steps 

Every step can be of a different type e.g. `url` requests a URL, `cmd` executes a system command. Every step may contain an `assert` instruction that evaluates a line of Python code. If the `assert` instruction returns `true`, the sequential execution stops and returns the string in `status`.

The `onexit` instruction can be set to `true` to make a step running at exit, to perform cleanup tasks.

The following section contains the step types:

##### **url**

Performs an HTTP request using Python Requests's  `requests.request()` [method](https://requests.readthedocs.io/en/master/api/#requests.request) and returns the `requests.Response` [object](https://requests.readthedocs.io/en/master/api/#requests.Response) as variable `res` to be evaluated in the `assert` section.  

Additional keys:

- `post_data`: dictionary with POST data passed to the Requests's `post` parameter.
- `post_json`: dictionary with JSON data passed to the Requests's `json` parameter.
- `headers`: dictionary with additional HTTP headers passed to the Requests's `headers` parameter.
- `post_files`: dictionary with files data passed to the  `files` parameter.
- `allow_redirect`: boolean passed to the Python Requests's `allow_redirect` HTTP Request parameter.
- `method`: string passed to Python Requests's  `method` parameter.

##### **cmd**

Executes a system command and returns the result in the variable `res` to be evaluated in the  `assert` section. 

The result is a dictionary with the keys: `stdout`, `stderr`, `retcode` .

##### **eval**

Executes code using Python's `exec()` [function](https://docs.python.org/2/reference/simple_stmts.html#exec).

##### **assert**

Evaluates code using Python's `eval` [function](https://docs.python.org/2/library/functions.html#eval). When it returns `true`, the sequential execution stops and returns the string in `status`. If it returns `false`, the next step is executed.

##### **drop_privileges**

Drop the privileges to the specified user so the following steps run with reduced privileges. It's recommended to drop privileges as first step.

#### Example

This is an example of a minimal `checks.yml` file that tests the presence of a XSS in the `/index` page.

```yaml
flags:
  xss-index:
    - status: vulnerable
      url: http://www.vulnerableapp.com/index.php/<img src=x onerror=alert(1);>
      assert: |
        '<img src=x onerror=alert(1);>' in res.text
    - status: fixed
      url: http://www.vulnerableapp.com/index.php/<img src=x onerror=alert(1);>
      assert: |
        '&lt;img src=x onerror=alert(1);&gt;' in res.text
    - status: broken-functionality
      assert: True
```



### Action checks

The `actions` field in the YAML file defines other tests related to the operating system status within the exercise's container.

Currently, it only supports the `app-status` check that should return a `running` or `stopped` status depending on whether the application server is currently running.



#### Example

This is an example of an health check of the application.

```yaml
actions:
  app-status:
    - status: running
      url: http://www.vulnerableapp.com/
      assert: |
        res and res.status_code == 200
    - status: stopped
      assert: True
```

